//----------------------------------------------------------------------------
// Interrupt driven UART receive functions.
//----------------------------------------------------------------------------

#define RxBufSize	8
#define FULLmask	1
#define LOSTmask	2
#define NotFULL		0
#define UART_NO_DATA	0x0100

typedef struct {
	volatile char ip;
	volatile char op;
	volatile char status;
	int buf[RxBufSize];
} RxBuf;

extern RxBuf RxBuf2master;
extern RxBuf RxBuf2slave;

void initRxBufMaster(void);
int getRxBufMaster(void);
int getRxBufMasterWait(void);

void initRxBufSlave(void);
int getRxBufSlave(void);
int getRxBufSlaveWait(void);

