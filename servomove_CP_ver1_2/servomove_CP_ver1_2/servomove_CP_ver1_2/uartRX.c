//----------------------------------------------------------------------------
// Interrupt driven UART receive functions.
//----------------------------------------------------------------------------

#include <m8c.h>        // part specific constants and macros
#include "PSoCAPI.h"    // PSoC API definitions for all User Modules

#include "uartRx.h"

RxBuf RxBuf2master={	0,0,NotFULL	};
RxBuf RxBuf2slave= {	0,0,NotFULL	};

void initRxBufMaster(void){
	RxBuf2master.ip=RxBuf2master.ip=0;
	RxBuf2master.status=NotFULL;
}
static void putRxBufMaster(int c){	// called from ISR only
	if (RxBuf2master.status&FULLmask){	// buffer is FULL.
		RxBuf2master.status|=LOSTmask;	// So, data is lost.
		return ;
	}
	if (c==0xff){
		
	}
	
	RxBuf2master.buf[RxBuf2master.ip++]=c;
	if (RxBuf2master.ip>=RxBufSize) RxBuf2master.ip=0;	// wrap round.
	if (RxBuf2master.ip==RxBuf2master.op) RxBuf2master.status|=FULLmask;	// set FULL flag.
}
int getRxBufMaster(void){	// called from normal application.
	int c;
	if (RxBuf2master.ip==RxBuf2master.op && !(RxBuf2master.status&FULLmask)) return UART_NO_DATA;	// ip==op && not full => buffer empty.
	M8C_DisableGInt;	// exclusive control is necessary.
	c=RxBuf2master.buf[RxBuf2master.op++];
	if (RxBuf2master.op>=RxBufSize) RxBuf2master.op=0;	// wrap round.
	RxBuf2master.status&=(~FULLmask);	// clear FULL flag.
	M8C_EnableGInt;
	return c;
}
int getRxBufMasterWait(void){
	int c;
	while ((c=getRxBufMaster())== UART_NO_DATA);
	return c;
}

void initRxBufSlave(void){
	RxBuf2slave.ip=RxBuf2slave.ip=0;
	RxBuf2slave.status=NotFULL;
}
static void putRxBufSlave(int c){	// called from ISR only
	if (RxBuf2slave.status&FULLmask){	// buffer is FULL.
		RxBuf2slave.status|=LOSTmask;	// So, data is lost.
		return ;
	}
	if (c==0xff){
		
	}
	RxBuf2slave.buf[RxBuf2slave.ip++]=c;
	if (RxBuf2slave.ip>=RxBufSize) RxBuf2slave.ip=0;	// wrap round.
	if (RxBuf2slave.ip==RxBuf2slave.op) RxBuf2slave.status|=FULLmask;	// set FULL flag.
}
int getRxBufSlave(void){	// called from normal application.
	int c;
	if (RxBuf2slave.ip==RxBuf2slave.op && !(RxBuf2slave.status&FULLmask)) return UART_NO_DATA;	// ip==op && not full => buffer empty.
	M8C_DisableGInt;	// exclusive control is necessary.
	c=RxBuf2slave.buf[RxBuf2slave.op++];
	if (RxBuf2slave.op>=RxBufSize) RxBuf2slave.op=0;	// wrap round.
	RxBuf2slave.status&=(~FULLmask);	// clear FULL flag.
	M8C_EnableGInt;
	return c;
}
int getRxBufSlaveWait(void){
	int c;
	while ((c=getRxBufSlave())== UART_NO_DATA);
	return c;
}
void RX8masterISR(void){
	putRxBufMaster(RX8_master_iReadChar());
}
void RX8slaveISR(void){
	putRxBufSlave(RX8_slave_iReadChar());
}

