//*****************************************************************************
//*****************************************************************************
//  FILENAME: TX8_master.h
//   Version: 3.50, Updated on 2015/3/4 at 22:27:52
//  Generated by PSoC Designer 5.4.3191
//
//  DESCRIPTION: TX8 User Module C Language interface file.
//-----------------------------------------------------------------------------
//      Copyright (c) Cypress Semiconductor 2015. All Rights Reserved.
//*****************************************************************************
//*****************************************************************************
#ifndef TX8_master_INCLUDE
#define TX8_master_INCLUDE

#include <m8c.h>

/* Create pragmas to support proper argument and return value passing */
#pragma fastcall16  TX8_master_SetTxIntMode
#pragma fastcall16  TX8_master_EnableInt
#pragma fastcall16  TX8_master_DisableInt
#pragma fastcall16  TX8_master_Start
#pragma fastcall16  TX8_master_Stop
#pragma fastcall16  TX8_master_SendData
#pragma fastcall16  TX8_master_bReadTxStatus

// High level TX functions
#pragma fastcall16  TX8_master_PutSHexByte
#pragma fastcall16  TX8_master_PutSHexInt
#pragma fastcall16  TX8_master_CPutString
#pragma fastcall16  TX8_master_PutString
#pragma fastcall16  TX8_master_PutChar
#pragma fastcall16  TX8_master_Write
#pragma fastcall16  TX8_master_CWrite
#pragma fastcall16  TX8_master_PutCRLF

//-------------------------------------------------
// Prototypes of the TX8_master API.
//-------------------------------------------------
extern void  TX8_master_SetTxIntMode(BYTE bTxIntMode);
extern void  TX8_master_EnableInt(void);
extern void  TX8_master_DisableInt(void);
extern void  TX8_master_Start(BYTE bParity);
extern void  TX8_master_Stop(void);
extern void  TX8_master_SendData(BYTE bTxData);
extern BYTE  TX8_master_bReadTxStatus(void);

// High level TX functions
extern void   TX8_master_CPutString(const char * szRomString);
extern void   TX8_master_PutString(char * szRamString);
extern void   TX8_master_PutChar(CHAR cData);
extern void   TX8_master_Write(char * szRamString, BYTE bCount);
extern void   TX8_master_CWrite(const char * szRomString, INT iCount);
extern void   TX8_master_PutSHexByte(BYTE bValue);
extern void   TX8_master_PutSHexInt(INT iValue);
extern void   TX8_master_PutCRLF(void);

// Old style function name, Do Not Use.
// Will be removfr in a future release
#pragma fastcall16 bTX8_master_ReadTxStatus
extern BYTE bTX8_master_ReadTxStatus(void);

//------------------------------------
//  Transmitter Parity masks
//------------------------------------
#define  TX8_master_PARITY_NONE         0x00
#define  TX8_master_PARITY_EVEN         0x02
#define  TX8_master_PARITY_ODD          0x06

//------------------------------------
//  Transmitter Status Register masks
//------------------------------------
#define  TX8_master_TX_COMPLETE         0x20
#define  TX8_master_TX_BUFFER_EMPTY     0x10

#define TX8_master_INT_MODE_TX_REG_EMPTY 0x00
#define TX8_master_INT_MODE_TX_COMPLETE  0x01

//------------------------------------
// Transmitter Interrupt masks
//------------------------------------
#define TX8_master_INT_REG_ADDR                ( 0x0e1 )
#define TX8_master_bINT_MASK                   ( 0x04 )

// Old style defines, do not use.  These
// will be removed in a future release.
#define  TX8_PARITY_NONE         0x00
#define  TX8_PARITY_EVEN         0x02
#define  TX8_PARITY_ODD          0x06
#define  TX8_TX_COMPLETE         0x20
#define  TX8_TX_BUFFER_EMPTY     0x10



//-------------------------------------------------
// Register Addresses for TX8_master
//-------------------------------------------------
#pragma ioport  TX8_master_CONTROL_REG: 0x02b              // Control register
BYTE            TX8_master_CONTROL_REG;
#pragma ioport  TX8_master_TX_SHIFT_REG:    0x028             // TX Shift Register register
BYTE            TX8_master_TX_SHIFT_REG;
#pragma ioport  TX8_master_TX_BUFFER_REG:   0x029             // TX Buffer Register
BYTE            TX8_master_TX_BUFFER_REG;
#pragma ioport  TX8_master_FUNC_REG:    0x128              // Function register
BYTE            TX8_master_FUNC_REG;
#pragma ioport  TX8_master_INPUT_REG:   0x129              // Input register
BYTE            TX8_master_INPUT_REG;
#pragma ioport  TX8_master_OUTPUT_REG:  0x12a              // Output register
BYTE            TX8_master_OUTPUT_REG;

#endif
// end of file TX8_master.h
