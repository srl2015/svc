//----------------------------------------------------------------------------
// C main line
//----------------------------------------------------------------------------

#include <m8c.h>        // part specific constants and macros
#include "PSoCAPI.h"    // PSoC API definitions for all User Modules
#include "uartRx.h"

#define MYADDRESS 		11
#define COMPARE         255
#define WRITE			0
#define READ 			1
#define WRITE_TO_ME		2
#define WRITE_TO_OTHER	3
#define READ_FROM_ME	4
#define READ_FROM_OTHER	5

void sleep113us(void){ // @clk=24MHz
	char i;
	for (i=0;i<100;i++);
}
	
void main (void){
	int i,sign,status,dust,address,pattern;
	char data[16],nod,check,test; //char->1byte
	
	///-----start-----///
	/// 
	
	DE_1_Start();
	DE_1_On();		// H
	DE_1_Off();		// L
	
	Counter8_1_Start();
	Counter8_2_Start();
	
	//LED_1_Start();
	//LED_1_On;			//LOW 
	//LED_1_Off;		//HIGH ->なんだかしらないけど逆なので注意
	//LED_1_On;			//LOW  ->なんだかしらないけど逆なので注意
	
	TX8_master_Start(TX8_PARITY_NONE);	// Enable TX8_master
	TX8_slave_Start(TX8_PARITY_NONE);	// Enable TX8_slave
	for(i=0; i<10000; i++)	sleep113us();
	
	RX8_master_Start(RX8_PARITY_NONE);	// Enable RX8_master
	RX8_slave_Start(RX8_PARITY_NONE);	// Enable RX8_slave
	
	for(i=0; i<100; i++)	sleep113us();
	
	M8C_EnableGInt;
	RX8_master_EnableInt();
	RX8_slave_EnableInt();
	
	for(i=0; i<100; i++)	sleep113us();

	/////変更点////
	while (RX8_master_bReadRxStatus()==RX8_RX_COMPLETE){
		dust=RX8_master_bReadRxData();
	}
	while (RX8_slave_bReadRxStatus()==RX8_RX_COMPLETE){
		dust=RX8_slave_bReadRxData();
	}
	
	///////ここまで
	while (1){
		DE_1_Off();		// L
	
		///----- receive signal from master -----///	
		sign=getRxBufMasterWait();
	
		///----- switch -----///
		address=sign>>1;
		if ((address&COMPARE)==MYADDRESS){
			if ((sign&READ)==READ)	pattern=READ_FROM_ME;
			else 					pattern=WRITE_TO_ME;
		}
		else {
			if ((sign&READ)==READ)	pattern=READ_FROM_OTHER;
			else 					pattern=WRITE_TO_OTHER;
		} 
	
		switch (pattern){
			
			case WRITE_TO_ME:	//write to me
				nod=getRxBufMasterWait();						  //receive num of data
				sleep113us();				
				for (i=0;i<nod;i++)	data[i]=getRxBufMasterWait(); //receive data
				TX8_slave_PutChar(WRITE);						  //send write signal to psoc
				TX8_slave_PutChar(nod);						  //send num of data to psoc
				sleep113us();									
				for (i=0;i<nod;i++)	TX8_slave_PutChar(data[i]);  //send data to PSoC
				sleep113us();
				check=getRxBufSlaveWait();						  //receive Wack (Write acknowledge)

				///----- send Wack -----///
				DE_1_On();		// H
				TX8_master_PutChar(check);
				sleep113us();
				DE_1_Off();		// L		
				
				break;
						
			case READ_FROM_ME:	//read from me
				nod=getRxBufMasterWait();						 //receive num of data
				TX8_slave_PutChar(READ);						 //send read signal to psoc
				TX8_slave_PutChar(nod);						 //send num of data to psoc
				sleep113us();
				for (i=0;i<nod;i++)	data[i]=getRxBufSlaveWait(); //recieve data from psoc
				
				///----- send data to master -----///
				DE_1_On();		// H
				for (i=0;i<nod;i++){	
					TX8_master_PutChar(data[i]);
					sleep113us();
				}
				DE_1_Off();		// L
				break;
				
			case WRITE_TO_OTHER:	//write to other
				nod=getRxBufMasterWait();						//receive num of data
				sleep113us();
				for (i=0;i<nod;i++)	dust=getRxBufMasterWait();	//receive data
				sleep113us();
				check=getRxBufMasterWait();						//receive data from other psoc
				break;
				
			case READ_FROM_OTHER:	//read from other
				nod=getRxBufMasterWait();						//receive num of data
				for (i=0;i<nod;i++)	dust=getRxBufMasterWait();	//recieve data from other
				break;
		}
	}
}
	
	