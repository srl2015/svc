#include <m8c.h>        // part specific constants and macros
#include "PSoCAPI.h"    // PSoC API definitions for all User Modules

BYTE  readBuffer[16]={0},writeBuffer[16]={0},PacketBuffer[16]={0};
BYTE  GainBit,mode,Ktau,choise;
WORD  pulseWidth,PWMperiod;
INT   Th1,Th2[10],Thr,DATA,DUST;
INT   e1,e2,Uw,c,i;
LONG  U,Up,Ud,u,umax,umin;
DWORD Kp,Kd;
BYTE  flag1;
BYTE  sign,num_of_recv,PacketSize;
BYTE  test,rxflag,rflag,wflag;		// rflag->read flag 　|　 wflag->write flag
BYTE  Th[2];

#define UART_NO_DATA	0x0100
#define UART_DATA_FULL  0x1000
#define WRITE			0
#define READ			1
#define NOT_RECV		2
#define NOT_WRITE		3		
#define NOT_READ		4
#define RX_COMPLETE		5
#define WRITE_COMPRITE	6
#define READ_COMPLETE	7
#define CHECK			42		//mbed と対応してないとだめ

void sleep113us(void){ // @clk=24MHz
	char i;
	for (i=0;i<100;i++);
}

int PacketRead(void){//mbed -> psoc  data read
	if (rxflag == NOT_RECV)	return UART_NO_DATA;
	else if ((rxflag == RX_COMPLETE) && (wflag == WRITE_COMPRITE)){
		M8C_DisableGInt;
		for (i=0; i<PacketSize; i++)	writeBuffer[i] = PacketBuffer[i+2];
		TX8_PutChar(CHECK);
		M8C_EnableGInt;
		rxflag = NOT_RECV;
		wflag = NOT_WRITE;
		return UART_DATA_FULL;
	}
}

void PacketWrite(void){//psoc -> mbed  data write 
	if ((rxflag == RX_COMPLETE) && (rflag == READ_COMPLETE)){
		char i;
		M8C_DisableGInt;
		for (i=0; i<PacketSize; i++)	TX8_PutChar(Th[i]);
		M8C_EnableGInt;
		rxflag = NOT_RECV;
		rflag = NOT_READ;
	}
}

static void putRxPacket(int c){		// called from ISR only
	if 		(num_of_recv==0)	sign=PacketBuffer[num_of_recv++]=c;//1byte目
	else if (sign == WRITE){//write
		if 		(num_of_recv==1)								PacketSize=PacketBuffer[num_of_recv++]=c;//2byte目
		else if (num_of_recv>=2 && num_of_recv<(PacketSize+2))	PacketBuffer[num_of_recv++] = c;
		if (num_of_recv >= (PacketSize+2)){//+2 -> sign data and nod data
			rxflag = RX_COMPLETE;
			wflag = WRITE_COMPRITE;
			num_of_recv = 0;
		}
	}
	else if (sign == READ){//read
		if (num_of_recv==1){
			PacketSize=PacketBuffer[num_of_recv++]=c;//2byte目
			rxflag = RX_COMPLETE;
			rflag = READ_COMPLETE;
			num_of_recv = 0;
		}
	}
}

void RX8ISR(void ){
	putRxPacket(RX8_iReadChar());
}

void main(void){	
	///----- initialized parameter -----/// 
	DATA=0;
    mode=3;
    Kp=300;			//best
    Kd=1000;		//best
    Ktau=3;
    umax=262143;	//0x0003FFFF
    umin=-262143;	//0xFFFC0001
	i=0;
    flag1=0;
	rxflag=NOT_RECV;
	num_of_recv=0;
	PacketSize=8;	//not 0
	
	///----- AD setting -----///
    PGA_1_Start(PGA_1_HIGHPOWER);
    DELSIG11_1_Start(DELSIG11_1_HIGHPOWER);
    DELSIG11_1_StartAD();
	
    ///----- UART setting -----///
	Counter8_1_Start();
	TX8_Start(TX8_PARITY_NONE);
	for (i=0; i<10000; i++)	sleep113us();////////約100ms
	RX8_Start(RX8_PARITY_NONE);
	for (i=0; i<100; i++)	sleep113us();
	M8C_EnableGInt;		//Turn on interrupts
	RX8_EnableInt();
	
	for (i=0; i<100; i++)	sleep113us();
	
	while (RX8_bReadRxStatus()==RX_COMPLETE){
		DUST=RX8_bReadRxData();
	}
	
	while(mode==3){
		if ((c=PacketRead())==UART_DATA_FULL){
			mode=(writeBuffer[1]&0xc0)>>6;
   			if(mode!=1) 					flag1=0;
    		if((writeBuffer[1]&0x04)==0)	DATA=((writeBuffer[1]&0x07)<<8)+writeBuffer[0];
    		else							DATA=((writeBuffer[1]|0xF8)<<8)+writeBuffer[0];
    		GainBit=(writeBuffer[1]&0x20)>>5;
    		if(GainBit==1){
    			Kp=(writeBuffer[3]<<8)+writeBuffer[2];
    			Kd=(writeBuffer[5]<<8)+writeBuffer[4];
    		}
		}
		while(DELSIG11_1_fIsDataAvailable()==0);          //sample rate=DataClock/1024 //1.024(msec)
			Th1=(DELSIG11_1_iGetData() & 0xffff);                   
        DELSIG11_1_ClearFlag();
        Th2[i]=Th1;
		Th[0]=(Th1 & 0x00ff);
	    Th[1]=(Th1 & 0x0700)>>8;
		i++;
        if(i>9)	i=0;
		PacketWrite();
	}
	
	///----- PWM setting -----/// 
	PWMDB16_EnableInt();
    PWMperiod=1023;
    pulseWidth=511;
    PWMDB16_WritePeriod(PWMperiod);
    PWMDB16_WritePulseWidth(pulseWidth);
    PWMDB16_Start();
	
	///----- Th default value -----///
    while(1){
		if ((c=PacketRead())==UART_DATA_FULL){
    		mode=(writeBuffer[1]&0xc0)>>6;
			if(mode==3) break;
   			if(mode!=1) flag1=0;
    		if((writeBuffer[1]&0x04)==0)	DATA=((writeBuffer[1]&0x07)<<8)+writeBuffer[0];
    		else							DATA=((writeBuffer[1]|0xF8)<<8)+writeBuffer[0];
    		GainBit=(writeBuffer[1]&0x20)>>5;
    		if(GainBit==1){
    			Kp=(writeBuffer[3]<<8)+writeBuffer[2];
    			Kd=(writeBuffer[5]<<8)+writeBuffer[4];
    		}
		}
		PacketWrite();
		
    	while(DELSIG11_1_fIsDataAvailable()==0);          //sample rate=DataClock/1024 //1.024(msec)
			Th1=(DELSIG11_1_iGetData() & 0xffff);
		DELSIG11_1_ClearFlag();
		Th[0]=(Th1 & 0x00ff);
		Th[1]=(Th1 & 0x0700)>>8;
		Th1=((Th1<<2) & 0xfffc);
			
    	if((mode==1) && (flag1==0)){
    		Thr=Th1;
    		flag1=1;
    	}
   		if		(mode==0)		Thr=(DATA<<2)&0xfffc;
   		else if	(mode==1)		Thr+=DATA;
    	if		(Thr>(850<<2))	Thr=(850<<2);
		else if	(Thr<(-850<<2)) Thr=(-850<<2);
    	e1=Thr-Th1;
        e2=Th1-Th2[i];
        Up=Kp*e1;
        Ud=Kd*e2;
        U=Up-Ud;
        Th2[i]=Th1;
        i++;
        if(i>9)	i=0;

        // pulse width output here.
        if		(umin>U)	u=umin;
        else if	(umax<U)	u=umax;
        else		    	u=U;
		
        Uw=(u>>9)&0x0000ffff;
        if(mode==2){
   			if		(DATA<0)	Uw=DATA+Ktau*e2-60;
   			else if	(DATA>0)	Uw=DATA+Ktau*e2+40;
   			else				Uw=0;
   			if	((Th1<(-850<<2)) || (Th1>(850<<2))) Uw=0;
   		}
        if		(Uw>486)	Uw=488;
        else if	(Uw<-486)	Uw=-488;	
		pulseWidth=Uw+511;
		PWMDB16_WritePulseWidth(pulseWidth);
	}
    DELSIG11_1_StopAD();
    DELSIG11_1_Stop();
	PWMDB16_Stop();
}
