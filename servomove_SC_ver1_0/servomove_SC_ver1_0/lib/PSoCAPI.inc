;  Generated by PSoC Designer 5.4.3191
;
; =============================================================================
; FILENAME: PSoCAPI.inc
;  
; Copyright (c) Cypress Semiconductor 2013. All Rights Reserved.
;  
; NOTES:
; Do not modify this file. It is generated by PSoC Designer each time the
; generate application function is run. The values of the parameters in this
; file can be modified by changing the values of the global parameters in the
; device editor.
;  
; =============================================================================
 
include "PSoCGPIOINT.inc"
include "Counter8_1.inc"
include "DELSIG11_1.inc"
include "PGA_1.inc"
include "PWMDB16.inc"
include "RX8.inc"
include "SleepTimer_1.inc"
include "TX8.inc"
